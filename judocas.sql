DROP SCHEMA IF EXISTS jud;
CREATE SCHEMA jud;
USE jud;
CREATE TABLE usuarios(
	idUser int auto_increment primary key, 
	email varchar(32) unique, 
	senha varchar(32),
	tipo varchar(18)
);
CREATE TABLE lutadores( 
	idLutador int auto_increment primary key, 
	nome varchar(30) not null,
	faixa varchar(8) not null 
);
CREATE TABLE hora(
	idHora int auto_increment primary key,
	minutos int (60),
	hora int(24) 
);
CREATE TABLE data( 
	idData int auto_increment primary key,
	mes varchar(12), 
	ano int(4), 
	dia varchar(30)
);
CREATE TABLE luta(
	idLuta int auto_increment primary key, 
	lutador1 varchar(80),
	lutador2 varchar(80),
	local varchar(40),
	idHora  int,
	idData int,
	foreign key (idHora) references hora(idHora),  
	foreign key (idData) references data(idData) 
);
CREATE TABLE pontuacao(
	idp int auto_increment primary key,
	tipo varchar(20)
);
