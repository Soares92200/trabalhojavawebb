<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>

<meta charset='UTF-8'>
<title>Luta</title>
<meta http-equiv='X-UA-Compatible' content='IE=edge'>
<meta name='viewport'
	content='width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no'>
<link rel='stylesheet' type='text/css'
	href='Visual/css/bootstrap.min.css'>
<link rel='stylesheet' type='text/css' href='Visual/css/style.css'>

<title>Luta</title>
</head>
<body>

<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="#">Judôx</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
    <div class="navbar-nav">
      <a class="nav-item nav-link active" href="index.jsp">Home<span class="sr-only">(current)</span></a>
      <a class="nav-item nav-link" href="login.jsp">Login</a>
      <a class="nav-item nav-link" href="cadastrar.jsp">Cadastro</a>
      <a class="nav-item nav-link" href="lutadores.jsp">Lutadores</a>
    </div>
  </div>
</nav>
		<div class="container h-100vh">
			<div class="inicio text-center">
				<h1 class="letra-1">Judôx</h1>
			</div>
			<div class="d-flex justify-content-center mt-4">
				<div class=" cc-1 card">
					<div class="card-body shadow">
						<form action="ServUsuario" method='post'>
							<div class="form-group row">
								<div class=" c-1 col-sm-5 text-center-flex">
									<label for="nome">Lutadores:</label>
								</div>
								<div class="col-sm-6">
									<input type="text" class="form-control" name='lutador1'
										placeholder="Lutador 1">
										<input type="text" class="form-control" name='lutador2'
										placeholder="Lutador 2">
								</div>
							</div>
							<div class="form-group row">
								<div class=" c-1 col-sm-5 text-center-flex">
									<label for="nome">Pontuação:</label>
								</div>
								<div class="col-sm-7">
									<input type="text" class="form-control" name='tipo'
										placeholder="Tipo">
								</div>
							</div>
							
							
							</form>
							
						
								
								
							
							<div class="d-flex center " style="padding: 1rem;">
								<input class="btn btn-primary btn-xl js-scroll-trigger"
									type='submit' name='enviar' value='Enviar' />

</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
