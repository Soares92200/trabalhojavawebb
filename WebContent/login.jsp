<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>

<meta charset='UTF-8'>
<title>Login</title>
<meta http-equiv='X-UA-Compatible' content='IE=edge'>
<meta name='viewport'
	content='width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no'>
<link rel='stylesheet' type='text/css'
	href='Visual/css/bootstrap.min.css'>
<link rel='stylesheet' type='text/css' href='Visual/css/style.css'>

<title>Login</title>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="#">Judôx</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
    <div class="navbar-nav">
      <a class="nav-item nav-link active" href="index.jsp">Home<span class="sr-only">(current)</span></a>
      <a class="nav-item nav-link" href="cadastrar.jsp">Cadastro</a>
    </div>
  </div>
</nav>
	<div class=" bg-container">
		<div class="container h-100vh">
			<div class="inicio text-center">
				<h1 class="letra">Judôx</h1>
			</div>
			<div class="d-flex justify-content-center mt-4">
				<div class=" cc-1 card">
					<div class="card-body shadow">
						<form action="ServUsuario" method='post'>
							<div class="form-group row">
								<div class=" c-1 col-sm-5 text-center-flex">
									<label for="email">Email:</label>
								</div>
								<div class="col-sm-5">
									<input type="text" class="form-control" name='email'
										placeholder="Email">
								</div>
							</div>
							<div class="form-group row">
								<div class=" c-1 col-sm-5 text-center-flex">
									<label for="senha">Senha:</label>
								</div>
								<div class="col-sm-5">
									<input type="text" class="form-control" name='senha'
										placeholder="Senha">
								</div>
							
								
								</div>
							
							<div class="d-flex justify-content-end " style="padding: 1rem;">
								<input class="btn btn-success btn-xl js-scroll-trigger"
									type='submit' name='submit' value='Cadastrar' />

							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
