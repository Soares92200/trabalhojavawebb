package controle;
import java.sql.PreparedStatement;
import modelo.Usuario;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class UsuarioControle {
	public boolean inserirUsuario(Usuario user) {
		boolean retorno = false;
		
	try {
		Conexao con = new Conexao();
		PreparedStatement ps= con.getCon().prepareStatement("INSERT INTO  usuarios(email,senha,tipo) VALUES(?,?,?);");
				ps.setString(1, user.getEmail());
				ps.setString(2,user.getSenha());
				ps.setString(3,user.getTipo());
				if(!ps.execute()) {
					retorno=true;
				}
	   con.fecharConexao();
	}
	   catch(SQLException e) {
		System.out.println("Erro de SQL:"+ e.getMessage());
	}catch(Exception e ) {
		System.out.println("Erro geral:"+ e.getMessage());

	}
	return retorno;
}
	public Usuario selecionarUm(Usuario user) {
		try {
			Usuario usuario=new Usuario();
			Conexao con=new Conexao();
			PreparedStatement ps=con.getCon().prepareStatement("SELECT * FROM usuarios WHERE email=?;");
			ps.setString(1,user.getEmail());
			if(ps.execute()) {
				ResultSet rs=ps.executeQuery();
				if(rs.next()) {
					usuario.setIdUser(rs.getInt("idUser"));
					usuario.setEmail(rs.getString("email"));
					usuario.setSenha(rs.getString("senha"));
					usuario.setTipo(rs.getString("tipo"));
				}
			}else {
				usuario=null;
			}
			con.fecharConexao();
			return usuario;
		}catch(Exception e) {
			System.out.println("Erro geral: "+e.getMessage());
			return null;
		}
	}
	
	
	public ArrayList<Usuario> selecionarTodos(){
		ArrayList<Usuario> lista=null;
		try {
			Conexao con=new Conexao();
			PreparedStatement ps=con.getCon().prepareStatement("SELECT * FROM usuarios;");
			if(ps.execute()) {
				ResultSet rs=ps.executeQuery();
				if(rs!=null) {
					lista=new ArrayList<Usuario>();
					while(rs.next()) {
						Usuario user=new Usuario();
						user.setIdUser(rs.getInt("idUser"));
						user.setEmail(rs.getString("email"));
						user.setSenha(rs.getString("senha"));
						user.setTipo(rs.getString("tipo"));
						lista.add(user);
					}
					con.fecharConexao();
					return lista;
				}else {
					con.fecharConexao();
					return lista;
				}
			}else {
				con.fecharConexao();
				return lista;
			}
		}catch(Exception e) {
			System.out.println("Erro geral: "+e.getMessage());
			return lista;
		}
	}
	
	public boolean atualizar(Usuario user,String email){
        try{
            Conexao con=new Conexao();
            PreparedStatement ps=con.getCon().prepareStatement("UPDATE usuarios SET usuario=?,email=?,senha=? WHERE email=?;");
            ps.setString(1,user.getEmail());
            ps.setString(2,user.getSenha());
            ps.setString(3,user.getTipo());
            if(!ps.execute()) {
	            con.fecharConexao();
	            return true;
            }else {
            	con.fecharConexao();
            	return false;
            }
        }catch(Exception e){
            System.out.println("Erro bonitinho: "+e.getMessage());
            return false;
        }
    }
	
	public boolean deletar(Usuario user){
        boolean retorno;
        try{
            Conexao con = new Conexao();
            PreparedStatement ps = con.getCon().prepareStatement("DELETE FROM usuarios WHERE email=?");
            ps.setString(1,user.getEmail());
            if(!ps.execute()){
                con.fecharConexao();
                retorno=true;
            }else{
                con.fecharConexao();
                retorno=false;
            }
        }catch(Exception e){
            System.out.println("Erro: "+ e.getMessage());
            retorno=false;
        }
        return retorno;
    }
	
}
