package controle;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import controle.UsuarioControle;
import modelo.Usuario;

/**
 * Servlet implementation class ServUsuario
 */
@WebServlet("/ServUsuario")
public class ServUsuario extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServUsuario() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Usuario user= new Usuario();
		user.setEmail(request.getParameter("email"));
		user.setSenha(request.getParameter("senha"));
		user.setTipo(request.getParameter("tipo"));
		if(new UsuarioControle().inserirUsuario(user)) {
			request.getRequestDispatcher("login.jsp").forward(request,response);
			
		}else {
			request.getRequestDispatcher("cadastro.jsp").forward(request,response)
;		}
	}

}

